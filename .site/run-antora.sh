#!/bin/sh
#
# When generating a developer redirect suitable for browsing as files
# rather than via a web server:
#
# * Do not indexify (--html-url-extension-style=indexify)
# * Use static redirects

antora/node_modules/.bin/antora --redirect-facility=static site.yml

#!/bin/sh

rm -r antora
curl -s https://gitlab.com/rshaull/antora/-/archive/auto-fill-links/antora-auto-fill-links.tar.gz | tar xvz
mv antora-auto-fill-links antora
(cd antora && yarn install)
